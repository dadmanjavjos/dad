package us.dad.rest;

import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Measurement {
	
	private static final AtomicInteger COUNTER = new AtomicInteger();
	private final int id;
	private final int
	private String location;
	private float temperature;
	private float humidity;
	private long date;				// La fecha es un long porque es un formato universal que entienden servidores, motores de BD, clientes etc.
	
	@JsonCreator
	public Measurement(@JsonProperty("location")String location, 
			@JsonProperty("temperature")float temperature, 
			@JsonProperty("humidity")float humidity, 
			@JsonProperty("date")long date) {
		super();
		this.id = COUNTER.getAndIncrement();
		this.location = location;
		this.temperature = temperature;
		this.humidity = humidity;
		this.date = date;			// La fecha entra por parámetro, porque la fecha la genera la placa.
		
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Measurement other = (Measurement) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}