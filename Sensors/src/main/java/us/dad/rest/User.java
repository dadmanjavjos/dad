package us.dad.rest;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

	private static final AtomicInteger COUNTER = new AtomicInteger();
	private final int id;
	private String user;
	private String email;
	private String pass;
	private Set<Board> boards;
	
	public User(@JsonProperty("user")String user,
			@JsonProperty("email")String email,
			@JsonProperty("pass")String pass){ 			// La contraseña entra por parámetro, porque la genera el cliente (codificándola en MD5). 	
		super();
		this.id = COUNTER.getAndIncrement();
		this.user = user;
		this.email = email;
		this.pass = pass;
		this.boards = new HashSet<Board>(null);
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public Set<Board> getBoards() {
		return boards;
	}

	public void setBoards(Set<Board> boards) {
		this.boards = boards;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
