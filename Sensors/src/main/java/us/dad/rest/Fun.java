package us.dad.rest;

import java.util.concurrent.atomic.AtomicInteger;

public class Fun {
	private static final AtomicInteger COUNTER = new AtomicInteger();
	private final int id;
	private boolean state;
	
	public Fun(){
		super();
		this.id = COUNTER.getAndIncrement();
		this.state = false;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fun other = (Fun) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
