package us.dad.rest;

import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Measurement {
	
	private static final AtomicInteger COUNTER = new AtomicInteger();
	private final int id;
//	private Board board;
	private String location;
	private float temperature;
	private float humidity;
	private long date;				// La fecha es un long porque es un formato universal que entienden servidores, motores de BD, clientes etc.
	
	@JsonCreator
	public Measurement(/*@JsonProperty("board")Board board,*/
			@JsonProperty("location")String location, 
			@JsonProperty("temperature")float temperature, 
			@JsonProperty("humidity")float humidity, 
			@JsonProperty("date")long date) {
		super();
		this.id = COUNTER.getAndIncrement();
//		this.board = board;
		this.location = location;
		this.temperature = temperature;
		this.humidity = humidity;
		this.date = date;			// La fecha entra por parámetro, porque la fecha la genera la placa.
		
	}

//	public Board getBoard() {
//		return board;
//	}
//
//	public void setBoard(Board board) {
//		this.board = board;
//	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Measurement other = (Measurement) obj;
		if (id != other.id)
			return false;
		return true;
	}

}