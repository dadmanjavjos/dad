package us.dad.rest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Map;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class RestVerticle extends AbstractVerticle {
	
	private Map<Integer, Measurement> measurements = new LinkedHashMap<>();
	private Map<Integer, Fun> funs = new LinkedHashMap<>();

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		super.start();
		Router router = Router.router(vertx);
		vertx.createHttpServer().requestHandler(router::accept).listen(8080);
		router.route("/api/temp*").handler(BodyHandler.create());
//		router.get("/api/temp/actual").handler(this::getActual);
		router.get("/api/temp/all").handler(this::getAll);
		router.put("/api/temp").handler(this::putOne);
		router.delete("/api/temp").handler(this::deleteOne); // Este método esta obsoleto.
		router.post("/api/temp/on:funId").handler(this::postOn);
		router.post("/api/temp/off:funId").handler(this::postOff);
		
//		router.put("/api/temp/putUser").handler(this::putUser);
//		router.put("/api/temp/putBoard").handler(this::putBoard);
		

	}

	@Override
	public void stop() throws Exception {
		// TODO Auto-generated method stub
		super.stop();
	}


	private void getAll(RoutingContext routingContext){
		routingContext.response().putHeader("content-type", "application/json;charset=utf-8")
		.end(Json.encodePrettily(measurements.values()));
	}
	private void putOne(RoutingContext routingContext){
		Measurement m = Json.decodeValue(routingContext.getBodyAsString(), Measurement.class);
		measurements.put(m.getId(), m);
		routingContext.response().setStatusCode(201)
		.putHeader("content-type", "application/json;charset=utf-8")
		.end(Json.encodePrettily(m));
	}
	private void deleteOne(RoutingContext routingContext){
		Measurement m = Json.decodeValue(routingContext.getBodyAsString(), Measurement.class);
		Measurement md = measurements.remove(m.getId());
		routingContext.response().setStatusCode(201).putHeader("content-type", "application/json;charset=utf-8").end(Json.encodePrettily(md));	

	}
	private void postOn(RoutingContext routingContext){
		int id  = Integer.parseInt(routingContext.request().getParam("funId"));
		funs.get(id).setState(true);
	}
	private void postOff(RoutingContext routingContext){
		int id  = Integer.parseInt(routingContext.request().getParam("funId"));
		funs.get(id).setState(false);
	}

//	private void getActual(RoutingContext routingContext){
//
//		Measurement m = actualMeasurementQuery();
//		routingContext.response().putHeader("content-type", "application/json;charset=utf-8")
//		.end(Json.encodePrettily(m));
//	}
//	private Measurement actualMeasurementQuery() {
//		Measurement m;
//		// Lo primero que tenemos que hacer es asegurarnos que el Driver de mysql se inicializa y se registra, para ello
//		try{
//		   Class.forName("com.mysql.jdbc.Driver");
//		} catch (Exception e){
//		   e.printStackTrace();
//		}
//		
//		// Establecemos la conexión con la base de datos. 
//		Connection conexion = DriverManager.getConnection ("jdbc:mysql://localhost/prueba","root", "clave");
//		/*
//El primer parámetro del método getConnection() es un String que contiene la url de la base de datos:
//jdb:mysql porque estamos utilizando un driver jdbc para MySQL, que es el que nos hemos bajado.
//localhost porque el servidor de base de datos, en mi caso, está en el mismo ordenador en el que voy a correr el prorama java.
//Aquí puede ponerse una IP o un nombre de máquina que esté en la red.
//prueba es el nombre de la base de datos que he creado dentro de mysql. 
//Se debe poner la base de datos dentro del servidor de MySQL a la que se quiere uno conectar. Es el nombre que pusimos cuando desde SQL hicimos create database prueba;
//Los otros dos parámetros son dos String.
//Corresponden al nombre de usuario y password para acceder a la base de datos. Al instalar MySQL se crea el usuario root y se pide la password para él. 
//Como no hemos creado otros usuarios, usaremos este mismo.
//		 */	
//		// Se le pide al pool una conexion libre
//
//		// Se hace una o más transacciones: select, update, insert, delete ...
//
//		// Se libera la conexión para su posible uso por otro hilo
//		conexion.close();
//		
//		// Preparamos la consulta 
//		Statement s = conexion.createStatement(); 
//		ResultSet rs = s.executeQuery ("SELECT * FROM table_name ORDER BY id DESC LIMIT 1");
//		
//		m = (Measurement) rs; // Esto puede petar, probablemente pete.
//		
//		return m;
//	}
}
