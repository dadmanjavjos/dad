package us.dad.rest;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Board {

	private static final AtomicInteger COUNTER = new AtomicInteger();
	private final int id;
	private User user;
	private IP ip;
	private String mac;
	private int port;
	
	
	public Board(@JsonProperty("id")int id,
			@JsonProperty("user")User user,
			@JsonProperty("ip")IP ip,
			@JsonProperty("mac")String mac,
			@JsonProperty("port")int port){
		super();
		this.id = id;
		this.user = user;
		this.ip = ip;
		this.mac = mac;
		this.port = port;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public IP getIp() {
		return ip;
	}


	public void setIp(IP ip) {
		this.ip = ip;
	}


	public String getMac() {
		return mac;
	}


	public void setMac(String mac) {
		this.mac = mac;
	}


	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public int getId() {
		return id;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
