// ESTA CLASE ESTA OBSOLETA
package us.dad.rest;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Humidity {
	
	private static final AtomicInteger COUNTER = new AtomicInteger();
	private final int id;
	private String location;
	private float value;

	public Humidity(@JsonProperty("location")String location, @JsonProperty("value")float value){
		super();
		this.id = COUNTER.getAndIncrement();
		this.location = location;
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Humidity other = (Humidity) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public int getId() {
		return id;
	}
	
	
}
