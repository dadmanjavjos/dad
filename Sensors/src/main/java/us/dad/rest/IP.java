package us.dad.rest;

import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IP {
	
	private static final AtomicInteger COUNTER = new AtomicInteger();
	private final int id;
	private Board board;
	private String address;
	private long date;				// La fecha es un long porque es un formato universal que entienden servidores, motores de BD, clientes etc.
	
	public IP(@JsonProperty("board")Board board,
			@JsonProperty("address")String address,
			@JsonProperty("date")long date) {
		super();
		this.id = COUNTER.getAndIncrement();
		this.board = board;
		this.address = address;
		this.date = date;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IP other = (IP) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
