package net.Socket;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class JavaClient {
	public static void main(String[] args){
		try{
			Socket socket = new Socket("localhost", 8083);
			BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
			InputStreamReader isr = new InputStreamReader(bis);
			
			int c;
			StringBuffer res = new StringBuffer();
			while((c = isr.read()) != 10){
				res.append((char) c);
			}
			System.out.println(res.toString());
			
			
			BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
			OutputStreamWriter osw = new OutputStreamWriter(bos);
			osw.write("Hola desde el cliente Java" + JavaClient.class.getName());
			osw.flush();
			
			
			
			// No me he enterado por que repetimos esto.
			bis = new BufferedInputStream(socket.getInputStream());
			isr = new InputStreamReader(bis);
			res = new StringBuffer();
			while((c = isr.read()) != 10){
				res.append((char) c);
			}
			System.out.println(res.toString());
			
			socket.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
